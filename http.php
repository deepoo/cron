<?php
require_once './vendor/autoload.php';

use Workerman\Worker;
use http\RequestResponse;

$http_worker = new Worker("http://0.0.0.0:9009");

$http_worker->count = 1;

$http_worker->onMessage = function($connection, $data)
{
    $connection->send((new RequestResponse())->run());
};

Worker::runAll();
