<?php
namespace http;

use common\logic\JobLogic;
use Cron\CronExpression;

class JobController
{
    public $L_job;

    public function __construct()
    {
        $this->L_job = new JobLogic();
    }

    public function jobs()
    {
        $jobs = $this->L_job->readJobs();
        return $this->fetchTpl('./http/view/jobs.php', ['jobs'=>$jobs, 'type_kv'=>JobLogic::$type, 'status_kv'=>JobLogic::$status]);
    }

    public function add()
    {
        if(isset($_POST['do']))
        {
            $_POST = array_map('trim', $_POST);
            $job = [
                'name'=>isset($_POST['name']) ? $_POST['name'] : '',
                'type'=>isset($_POST['type']) ? $_POST['type'] : '',
                'command'=>isset($_POST['command']) ? $_POST['command'] : '',
                'status'=>'off',//新增任务，默认不开启
                'cron'=>[
                    'i'=>isset($_POST['cron_i']) ? $_POST['cron_i'] : '',
                    'h'=>isset($_POST['cron_h']) ? $_POST['cron_h'] : '',
                    'd'=>isset($_POST['cron_d']) ? $_POST['cron_d'] : '',
                    'm'=>isset($_POST['cron_m']) ? $_POST['cron_m'] : '',
                    'w'=>isset($_POST['cron_w']) ? $_POST['cron_w'] : '',
                    'y'=>isset($_POST['cron_y']) ? $_POST['cron_y'] : '',
                ]
            ];
            //验证是否合法
            if( ! CronExpression::isValidExpression(implode(' ', $job['cron']))){
                return $this->L_job->rs(1, 'Cron表达式错误');
            }
            //取出所有任务
            $jobs = $this->L_job->readJobs();
            //添加到最后
            $jobs[] = $job;
            //保存任务
            return $this->L_job->writeJobs($jobs);
        }
        return $this->fetchTpl('./http/view/add_edit.php', ['ae'=>'add', 'type_select'=>JobLogic::getTypeSelect('http_get')]);
    }

    public function edit()
    {
        if(isset($_POST['do']))
        {
            $_POST = array_map('trim', $_POST);
            $n = (int)$_POST['n'];
            $update_job = [
                'name'=>isset($_POST['name']) ? $_POST['name'] : '',
                'type'=>isset($_POST['type']) ? $_POST['type'] : '',
                'command'=>isset($_POST['command']) ? $_POST['command'] : '',
                'status'=>'off',
                'cron'=>[
                    'i'=>isset($_POST['cron_i']) ? $_POST['cron_i'] : '',
                    'h'=>isset($_POST['cron_h']) ? $_POST['cron_h'] : '',
                    'd'=>isset($_POST['cron_d']) ? $_POST['cron_d'] : '',
                    'm'=>isset($_POST['cron_m']) ? $_POST['cron_m'] : '',
                    'w'=>isset($_POST['cron_w']) ? $_POST['cron_w'] : '',
                    'y'=>isset($_POST['cron_y']) ? $_POST['cron_y'] : '',
                ]
            ];
            //验证是否合法
            if( ! CronExpression::isValidExpression(implode(' ', $update_job['cron']))){
                return $this->L_job->rs(1, 'Cron表达式错误');
            }
            //取出所有任务
            $jobs = $this->L_job->readJobs();
            if(empty($jobs)){ return $this->L_job->rs(1, '没有找到任务信息'); }
            foreach($jobs as $k=>$job){
                if($k === $n){
                    //更新
                    $update_job['status'] = $job['status'];
                    $jobs[$k] = $update_job;
                    break;
                }
            }
            //保存任务
            return $this->L_job->writeJobs($jobs);
        }

        $n = (int)$_GET['n'];
        $info = null;
        $jobs = $this->L_job->readJobs();
        if( ! empty($jobs)){
            foreach($jobs as $k=>$job){
                if($k === $n){
                    $info = $job;
                    break;
                }
            }
        }
        if(empty($info)){ return '<h3>没有找到任务信息</h3>'; }
        return $this->fetchTpl('./http/view/add_edit.php', ['ae'=>'edit', 'n'=>$n, 'info'=>$info, 'type_select'=>JobLogic::getTypeSelect($info['type'])]);
    }

    public function st()
    {
        if(isset($_POST['k']) && intval($_POST['k'])>-1){
            $k = intval($_POST['k']);
        }else{
            return $this->L_job->rs(1, '编号错误');
        }
        if(isset($_POST['st']) && array_key_exists($_POST['st'], JobLogic::$status)){
            $st = $_POST['st'];
            if($st == 'on'){ $st = 'off'; }
            else{ $st = 'on'; }
        }else{
            return $this->L_job->rs(1, '状态错误');
        }
        $jobs = $this->L_job->readJobs();
        foreach($jobs as $id=>&$v){
            if($id === $k){
                $v['status'] = $st;
                break;
            }
        }
        return $this->L_job->writeJobs($jobs);
    }

    public function del()
    {
        if(isset($_POST['n']) && intval($_POST['n'])>-1){
            $n = intval($_POST['n']);
        }else{
            return $this->L_job->rs(1, '编号错误');
        }
        $jobs = $this->L_job->readJobs();
        foreach($jobs as $k=>$v){
            if($k === $n){
                unset($jobs[$k]);
                break;
            }
        }
        $jobs = array_values($jobs);
        return $this->L_job->writeJobs($jobs);
    }

    public function miss()
    {
        return '404xxx';
    }

    public function wiki()
    {
        return $this->fetchTpl('./http/view/wiki.php');
    }

    private function fetchTpl($filename='', $data=[])
    {
        if(is_file($filename))
        {
            if(is_array($data) && count($data)>0){ extract($data); }
            ob_start();
            include $filename;
            return ob_get_clean();
        }
        return '';
    }
}