<form id="fm">
    <?php if($ae == 'edit'){ ?>
    <input type="hidden" id="n" value="<?php echo $n;?>" />
    <?php } ?>
    <table class="table table-bordered table-condensed">
        <tbody>
            <tr>
                <td class="warning" style="width:80px;">任务名称：</td>
                <td colspan="7"><input type="text" class="form-control" id="name" placeholder="输入任务的名称" value="<?php if($ae == 'edit'){ echo $info['name']; } ?>" /></td>
            </tr>
            <tr>
                <td class="warning">请求类型：</td>
                <td colspan="7"><?php echo $type_select; ?></td>
            </tr>
            <tr>
                <td class="warning">网络地址：</td>
                <td colspan="7"><input type="text" class="form-control" id="command" placeholder="http://www.test.me/api/xxx" value="<?php if($ae == 'edit'){ echo $info['command']; } ?>" /></td>
            </tr>
            <tr>
                <td class="warning" rowspan="2">定时：</td>
                <td class="success">分</td>
                <td class="success">时</td>
                <td class="success">月份的【天】</td>
                <td class="success">月份</td>
                <td class="success">星期的【天】</td>
                <td class="success">年</td>
            </tr>
            <tr>
                <td><input type="text" autocomplete="off" class="form-control" id="cron_i" value="<?php if($ae == 'edit'){ echo $info['cron']['i']; } ?>" /></td>
                <td><input type="text" autocomplete="off" class="form-control" id="cron_h" value="<?php if($ae == 'edit'){ echo $info['cron']['h']; } ?>" /></td>
                <td><input type="text" autocomplete="off" class="form-control" id="cron_d" value="<?php if($ae == 'edit'){ echo $info['cron']['d']; } ?>" /></td>
                <td><input type="text" autocomplete="off" class="form-control" id="cron_m" value="<?php if($ae == 'edit'){ echo $info['cron']['m']; } ?>" /></td>
                <td><input type="text" autocomplete="off" class="form-control" id="cron_w" value="<?php if($ae == 'edit'){ echo $info['cron']['w']; } ?>" /></td>
                <td><input type="text" autocomplete="off" class="form-control" id="cron_y" value="<?php if($ae == 'edit'){ echo $info['cron']['y']; } ?>" /></td>
            </tr>
            <tr>
                <td class="warning"></td>
                <td colspan="6" class="text-right"><span onclick="checkFM('<?php echo $ae; ?>')" class="btn btn-danger"><i class="icon icon-check"></i> 提交</span></td>
            </tr>
        </tbody>
    </table>
</form>
