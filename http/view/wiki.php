<style type="text/css">
    .ttb td{ text-indent: 1em; }
</style>
<div class="panel">
    <!--<div class="panel-heading">Linux Cron 说明</div>-->
    <div class="panel-body">
        <div>
            一个cron表达式有至少6个（也可能是7个）由空格分隔的时间元素。从左至右，这些元素的定义如下：<br><br>
            1．分钟（0–59）<br>
            2．小时（0–23）<br>
            3．月份中的日期（1–31）<br>
            4．月份（1–12）<br>
            5．星期中的日期（1–7）<br>
            6．年份（1970–2099）<br><br>
            每一个元素都可以显式地规定一个值（如6），一个区间（如9-12），<br>
            一个列表（如9，11，13）或一个通配符（如*）。<br>
            “月份中的日期”和“星期中的日期” 这两个元素是互斥的。<br>
        </div>
        <hr>
        <div>
            <p>下表中显示了一些cron表达式的例子和它们的意义：</p>
            <table class="table table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>分</th>
                    <th>时</th>
                    <th>月份的【天】</th>
                    <th>月份</th>
                    <th>星期的【天】</th>
                    <th>年</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>*</td>
                        <td>10,14,16</td>
                        <td>*</td>
                        <td>*</td>
                        <td>*</td>
                        <td>*</td>
                        <td>每天上午10点，下午2点和下午4点的每分钟</td>
                    </tr>
                    <tr>
                        <td>0,15,30,45</td>
                        <td>*</td>
                        <td>1-10</td>
                        <td>*</td>
                        <td>*</td>
                        <td>*</td>
                        <td>每月前10天每隔15分钟</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>0</td>
                        <td>1</td>
                        <td>1</td>
                        <td>*</td>
                        <td>*</td>
                        <td>在2018年1月1日0点1分</td>
                    </tr>
                    <tr>
                        <td>*/2</td>
                        <td>*</td>
                        <td>*</td>
                        <td>*</td>
                        <td>*</td>
                        <td>*</td>
                        <td>每 2 分钟</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
