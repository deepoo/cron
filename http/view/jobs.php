<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>计划任务</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.1/css/zui.min.css">
    <style type="text/css">.c_p{ cursor: pointer; }</style>
</head>
<body>
<div>
    <div class="btn-group">
        <span class="btn btn-danger" onclick="addJob()"><i class="icon icon-plus"></i> 添加任务</span>
        <span class="btn btn-warning" onclick="readWiki()"><i class="icon icon-file-o"></i> Linux Cron 知识</span>
    </div>
    <table class="table table-bordered table-hover table-condensed" style="margin-top: 10px;">
        <thead>
            <tr>
                <th>#</th>
                <th>任务</th>
                <th>状态</th>
                <th>命令</th>
                <th>分</th>
                <th>时</th>
                <th>月份的【天】</th>
                <th>月份</th>
                <th>星期的【天】</th>
                <th>年</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
        <?php if( ! empty($jobs)){ foreach($jobs as $k=>$job){ ?>
            <tr>
                <td><?php echo $k; ?></td>
                <td><strong><?php echo $job['name']; ?></strong></td>
                <td><span onclick="jobStatus(<?php echo $k; ?>, '<?php echo $job['status']; ?>')" class="c_p <?php echo $status_kv[$job['status']]['c']; ?>"><?php echo $status_kv[$job['status']]['v']; ?></span></td>
                <td><?php echo $job['command']; ?></td>
                <td><?php echo $job['cron']['i']; ?></td>
                <td><?php echo $job['cron']['h']; ?></td>
                <td><?php echo $job['cron']['d']; ?></td>
                <td><?php echo $job['cron']['m']; ?></td>
                <td><?php echo $job['cron']['w']; ?></td>
                <td><?php echo $job['cron']['y']; ?></td>
                <td>
                    <div class="btn-group">
                        <span class="btn btn-xs btn-info" onclick="editJob(<?php echo $k; ?>)"><i class="icon icon-pencil"></i> 编辑</span>
                        <span class="btn btn-xs btn-danger" onclick="jobDelete(<?php echo $k; ?>)"><i class="icon icon-remove"></i> 删除</span>
                    </div>
                </td>
            </tr>
        <?php }}else{ ?>
            <tr>
                <td colspan="12">...</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script src="//cdn.bootcss.com/zui/1.8.1/lib/jquery/jquery.js"></script>
<script src="//cdn.bootcss.com/zui/1.8.1/js/zui.min.js"></script>
<script>
    //新增任务弹窗
    function addJob()
    {
        (new $.zui.ModalTrigger({'title': '添加任务', 'moveable': true, 'width': 800, 'url': '/job/add'})).show();
    }
    
    //编辑任务弹窗
    function editJob(n)
    {
        (new $.zui.ModalTrigger({'title': '编辑任务', 'moveable': true, 'width': 800, 'url': '/job/edit?n='+n})).show();
    }

    //任务状态
    function jobStatus(k, st)
    {
        if( ! confirm("确认？")){ return; }
        $.ajax({
            url: '/job/st',
            data: {'k': k, 'st': st},
            type: 'post',
            dataType: 'json',
            beforeSend: function(){
                maskLoading();
            },
            success: function(msg){
                if(msg.c === 0){
                    greenTip('操作成功');
                    setTimeout(function(){ rl(); }, 700);
                }else{
                    redTip(msg.m);
                    clearMaskLoading();
                }
            },
            error: function(){
                alert('网络繁忙，请稍后重试。');
            }
        });
    }

    //删除任务
    function jobDelete(n)
    {
        if( ! confirm("确认？")){ return; }
        $.ajax({
            url: '/job/del',
            data: {'n': n},
            type: 'post',
            dataType: 'json',
            beforeSend: function(){
                maskLoading();
            },
            success: function(msg){
                if(msg.c === 0){
                    greenTip('操作成功');
                    setTimeout(function(){ rl(); }, 700);
                }else{
                    redTip(msg.m);
                    clearMaskLoading();
                }
            },
            error: function(){
                alert('网络繁忙，请稍后重试。');
            }
        });
    }

    //Linux Cron 知识
    function readWiki()
    {
        (new $.zui.ModalTrigger({'title': 'Linux Cron 知识', 'moveable': true, 'width': 800, 'url': '/wiki'})).show();
    }

    function checkFM(w)
    {
        var dts = {};
        dts.do = 'yes';

        var uu = '/job/add';

        if(w == 'edit'){
            var n = $.trim($('#n').val());
            n = parseInt(n);
            if(isNaN(n) || n<0){ redTip('参数错误'); return; }
            dts.n = n;
            uu = '/job/edit';
        }

        var name = $.trim($('#name').val());
        if(name == ''){ redTip('请输入任务名称'); return; }
        var type = $.trim($('#type').val());
        if(type == ''){ redTip('请选择任务类型'); return; }
        var command = $.trim($('#command').val());
        if(command == ''){ redTip('请输入命令'); return; }
        //定时
        var cron_i = $.trim($('#cron_i').val());
        if(cron_i == ''){ redTip('请输入【分】'); return; }
        var cron_h = $.trim($('#cron_h').val());
        if(cron_h == ''){ redTip('请输入【时】'); return; }
        var cron_d = $.trim($('#cron_d').val());
        if(cron_d == ''){ redTip('请输入【月份的天】'); return; }
        var cron_m = $.trim($('#cron_m').val());
        if(cron_m == ''){ redTip('请输入【月】'); return; }
        var cron_w = $.trim($('#cron_w').val());
        if(cron_w == ''){ redTip('请输入【星期的天】'); return; }
        var cron_y = $.trim($('#cron_y').val());
        if(cron_y == ''){ redTip('请输入【年】'); return; }
        //post参数
        dts.name = name;
        dts.type = type;
        dts.command = command;
        dts.cron_i = cron_i;
        dts.cron_h = cron_h;
        dts.cron_d = cron_d;
        dts.cron_m = cron_m;
        dts.cron_w = cron_w;
        dts.cron_y = cron_y;
        //console.log(dts); return;
        $.ajax({
            url: uu,
            data: dts,
            type: 'post',
            dataType: 'json',
            beforeSend: function(){
                maskLoading();
            },
            success: function(msg){
                if(msg.c === 0){
                    greenTip('操作成功');
                    setTimeout(function(){ rl(); }, 700);
                }else{
                    redTip(msg.m);
                    clearMaskLoading();
                }
            },
            error: function(){
                alert('网络繁忙，请稍后重试。');
            }
        });
    }

    //遮罩层
    function maskLoading(color)
    {
        if( ! color){ color='#ffffff'; }
        var tplLoading = $('<div id="maskLoading"><p><img src="http://om.i-sanger.cn/img/om/loading_c.gif"></p></div>');
        tplLoading.css({
            'width': '100%',
            'height': $(document).height()+'px',
            'position': 'absolute',
            'top': '0px',
            'left': '0px',
            'z-index': '3001',
            'background-color': color,
            'filter': 'alpha(opacity=50)',
            'opacity': '0.5'
        });
        var _top = ($(window).scrollTop() + $(window).height()/2 - 60)+'px';
        tplLoading.children('p').css({"text-align": "center", "padding-top": _top});
        $("body").prepend(tplLoading);
    }

    //清除遮罩层
    function clearMaskLoading()
    {
        $('#maskLoading').remove();
    }

    //刷新页面
    function rl()
    {
        window.location.reload(true);
    }

    //成功提示消息
    function greenTip(msg)
    {
        new $.zui.Messager(msg, {
            type: 'success',
            placement: 'center'
        }).show();
    }

    //失败提示消息
    function redTip(msg)
    {
        new $.zui.Messager(msg, {
            type: 'danger',
            placement: 'center'
        }).show();
    }
</script>
</body>
</html>
