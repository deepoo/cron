<?php
date_default_timezone_set('Asia/Shanghai');

include_once 'vendor/autoload.php';

//调试输出
function ddd($var, $exit=false, $print_r=false)
{
    if(headers_sent() === false){ header("Content-type:text/html; charset=utf-8"); }
    if($print_r){
        echo '<pre>';print_r($var);echo '</pre>';
    }else{
        ob_start();
        var_dump($var);
        $output = ob_get_clean();
        if(!extension_loaded('xdebug')){
            $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
            $output = '<pre>'. htmlspecialchars($output, ENT_QUOTES).'</pre>';
        }
        echo $output;
    }
    if ($exit){exit;}
    else{return;}
}

//$r = preg_match('/^\d+[,\d+]*/', '1,a,,3,4,5');
//$r = preg_match('/^\d+(,\d+)*$/', '1,2222,2222,a,2222');
//$r = preg_match('/^\d{4}(,\d{4})*$/', '1914,1975,2010');
//ddd($r,1);

/*include './common/logic/JobLogic.php';
$L_job = new \common\logic\JobLogic();
$jobs = $L_job->readJobs();
$ck = $L_job->isValidJob($jobs[0]);
ddd($ck);
ddd($L_job->getError(),1,1);*/

//$rule = '*/2.2 17,19 * * 2 *';
$rule = '* 8,9,10 * * * *';
if(\Cron\CronExpression::isValidExpression($rule)){
    $cron = \Cron\CronExpression::factory($rule);
    $is_due = $cron->isDue();
    ddd($is_due,1);
}else{
    ddd('表达式错误');
}

/*include './common/logic/JobLogic.php';
$L_job = new \common\logic\JobLogic();
$jobs = $L_job->readJobs();
$crons = [];
foreach($jobs as $job){
    //$crons[] = implode(' ', $job['cron']);
    $crons[] = $job['command'];
}
ddd($crons,1,1);*/
