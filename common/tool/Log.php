<?php
namespace common\tool;

class Log
{
    /**
     * 写入日志格式
     *
     * @param string $c
     *
     * @return void
     */
    public static function write($c = '')
    {
        $dir = './common/log/';
        $save_name = 'cron_'.date('Ymd').'.log';
        if( ! is_dir($dir)){
            mkdir($dir);
        }
        if( ! is_scalar($c)){
            $c = serialize($c);
        }
        error_log($c, 3, $dir.$save_name);
    }

    /**
     * 整理日志格式
     *
     * @param string $lg
     * @param string $extra
     *
     * @return string
     */
    public static function logFormat($lg = '', $extra = '')
    {
        $rs = '---------------------------------'.PHP_EOL;
        $rs .= date('Y-m-d H:i:s').PHP_EOL;
        $rs .= $lg.PHP_EOL;
        if( ! is_scalar($extra)){
            $extra = serialize($extra);
        }
        $rs .= $extra.PHP_EOL.PHP_EOL;
        return $rs;
    }
}
