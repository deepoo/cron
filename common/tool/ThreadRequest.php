<?php
namespace common\tool;

use Thread;
use GuzzleHttp\Client;

class ThreadRequest extends Thread
{
    public $url;
    public $method;

    public function __construct($url = '', $method = 'GET')
    {
        $this->url = $url;
        $this->method = $method;
    }

    /**
     * 请求url
     *
     * @throws
     *
     * @return string
     */
    public function run()
    {
        $client = new Client();
        $rs = $client->request($this->method, $this->url);
        $cc = $rs->getBody()->getContents();
        Log::write(Log::logFormat(['type'=>$this->method, 'command'=>$this->url], $cc));
    }
}
